# DSM Workflow Seedling

![logo](seedling_logo.png){width="150"}

## Abstract
A simplified R-based Digital Soil Mapping workflow developed at [ISRIC - World Soil Information](www.isric.org).


This workflow starts from standardised inputs, it organises data for modelling, performs modelling with Random Forest and creates soil maps as final outputs, including pixel based uncertainty.

It is a file-based workflow with each step needing specific files as inputs and creating specific files as outputs. Files are named and organised according to the main variables/options chosen by the user. A configuration file allows the user to keep track of the chosen options.

The full documentation for this workflow can be found 
[here](https://shiny.wur.nl/content/b81c4048-a784-4598-8087-8ad63cdc8df5). Please follow the instructions to set it up and run it. 

In this file you can find a summary of the main information for the installation and use of the workflow.

## Installation

Install the isric.dsm.base R package. This package and its system 
dependencies are the only requirement for the workflow to run.

Within an R session, you can use the following options

-   Directly from the git repository link

``` r
if (!require("remotes")) install.packages("remotes")
remotes::install_git('https://git.wur.nl/isric/dsm-general/dsm.workflows/seedling.git', subdir = "isric.dsm.base",ref="0.3.13")
```

-   Or by first cloning the repository with `git clone --depth 1 --branch 0.3.13 https://git.wur.nl/isric/dsm-general/dsm.workflows/seedling.git`, and then running

``` r
if (!require("devtools")) install.packages("devtools")
devtools::install("./isric.dsm.base")
```

-   Or by using the [tar.gz](https://git.wur.nl/isric/dsm-general/dsm.workflows/seedling/-/blob/0.3.13/isric.dsm.base_0.3.13.tar.gz) file contained in the repository

``` r
if (!require("remotes")) install.packages("remotes")
remotes::install_local('isric.dsm.base_0.3.13.tar.gz')
```

## Use

To run the workflow you will need to:

-   Modify the [config file](template.config.yaml)
-   Run the [run_workflow.R](run_workflow.R) script

### Configuration file

A [configuration file template](template.config.yaml) is provided.
This template must be modified according to the specific needs of the user.

The file contains:

-   Files and directories paths (input, output)

-   Workflow parameters (variable names, model options).

All these values need to be checked to ensure that 
the workflow can run without errors.

### Running the workflow

Once the configuration file has been filled-in and fully revised and the R 
package isric.dsm.base is installed the workflow can be executed.

To execute all the steps of the workflow 
run: [run_workflow.R](run_workflow.R). 
Make sure that the variable "config_location" is set to the correct location 
of the configuration file. By default this is "template.config.yaml".

#### Running the run_workflow.R script

After modifying the config file 
to match appropriate input files and parameters the script can be run interactively 
by using an IDE (e.g. RStudio or VSCode). The workflow script can aslo be called 
from the command line as follows 
- on Windows: 
`C:\Program Files\R\R-3.4.3\bin\Rscript.exe run_workflow.R`
 (note you might need to modify this example file path so that it 
points to the location of Rscript.exe on your computer) 
- on Linux: `Rscript run_workflow.R`

You can also give the configuration file location 
as a command line argument as follows:
`Rscript run_workflow.R --config_file path/to/config_file`

## Authors

- [ISRIC - World Soil Information - Giulio Genova](mailto:giulio.genova@isric.org)
- [ISRIC - World Soil Information - Larua Poggio](mailto:laura.poggio@isric.org)
- [ISRIC - World Soil Information - Bas Kempen](mailto:bas.kempen@isric.org)
- [ISRIC - World Soil Information - Betony Colman](mailto:betony.colman@isric.org)

## Point of contact

- [ISRIC - World Soil Information - Giulio Genova](mailto:giulio.genova@isric.org)   
 PO Box 353 , Wageningen , 6700AJ , Netherlands
- [ISRIC - World Soil Information - Laura Poggio](mailto:laura.poggio@isric.org)   
 PO Box 353 , Wageningen , 6700AJ , Netherlands

## License

This software is distributed under the GNU GENERAL PUBLIC LICENSE Version 3.   
The licence can be found in [LICENSE](LICENSE)

## Online resource
- [Repository](https://git.wur.nl/isric/dsm-general/dsm.workflows/seedling/-/tree/0.3.13)
- [Documentation](https://shiny.wur.nl/content/b81c4048-a784-4598-8087-8ad63cdc8df5)

## Disclaimer

The code is provided without warranty. ISRIC is not obliged to provide user 
support, updates or “bug fixes” of any kind.

## Feedback, bugs and questions

Feedback and bug reports are welcome. Please contact us at [dsm at isric dot org](mailto:dsmisric.org)

## Citation
Genova, G., Poggio, L., Kempen, B., & Colman, B. (2024). DSM Workflow Seedling. ISRIC - World Soil Information.
[https://doi.org/10.17027/ISRIC-FSX2-2691](https://doi.org/10.17027/ISRIC-FSX2-2691)