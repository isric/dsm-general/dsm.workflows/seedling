#' Predict quantile regression tiles using a fitted random forest model
#'
#' Given a fitted quantile regression forest model, this function predicts values for a single tile.
#'
#' @param tile_number integer specifying the tile number
#' @param voi Name of the variable of interest in the dataset.
#' @param tilesFile character string giving the path to a csv file containing information about the tiles to be predicted.
#' @param covarsDir character string giving the path to the directory containing covariate rasters.
#' @param rfeCovariatesFile character string giving the path to the csv file containing the covariate names selected using recursive feature elimination (RFE).
#' @param maskFile character string giving the path to a mask raster file. Default is NULL.
#' @param modelFittedFile character string giving the path to a fitted model object.
#' @param outputDir Character. Optional. Used if input and or output files are not given
#' @param transformation character string specifying the type of transformation to apply to the predicted values. Default is "notransform".
#' @param multiply numeric scalar to multiply the predicted values by. Default is 1.
#' @param tilesDir character string giving the path to the directory where predicted tiles will be written.
#' @param gdal_options character string giving additional options to pass to GDAL.
#' @param datatype_geotiff character string giving the data type of the output GeoTIFF file.
#' @param depths_3D list specifying the depths to predict at. Default is NULL.
#' @param covariate_pattern character string specifying a regular expression pattern to match the covariate files. Default is ".tif$".
#' @param NA_mask_flag A numeric value to be used as NA flag for the mask
#' @param use_rfe Specifies whether to apply Recursive Feature Elimination. Options are "dorfe" for RFE or "norfe" for no RFE.
#' @param depth_name Name of the column containing the depth information in the regression matrix
#' @param do_tune Specifies whether to tune hyperparameters. Options are "tune" or "notune".
#' @param rndseed Numeric. Value used to initialize the random number generator
#' @param model_name Character. name of the model used. Default to "rangerquantreg"
#' @param model_options list of additional options to control the behavior of the quantile regression forest model.
#' @param overwrite logical. if TRUE overwrite outputs. if FALSE function exits if all outpus are present. Default to TRUE
#'
#' @return Writes a raster file to disk with the predicted values
#'
#'
#' @importFrom dplyr across all_of mutate bind_cols
#' @importFrom tools file_path_sans_ext
#' @importFrom utils read.csv
#' @importFrom terra as.data.frame crs ext mask rast resample window writeRaster trim extend
#' @importFrom tidyr pivot_longer
#' @importFrom magrittr %>%
#' @importFrom rlang enquo
#' @importFrom sf st_as_sf
#'
#' @export
#'

predict_tile <- function(
    tile_number,
    voi,
    tilesFile = "{outputDir}/other/tiles.csv",
    covarsDir,
    rfeCovariatesFile = "{outputDir}/model/{voi}/rfe_{depth_name}_{transformation}.csv",
    maskFile,
    modelFittedFile = "{outputDir}/model/{voi}/model-fitted_{model_name}_{depth_name}_{transformation}_{use_rfe}_{do_tune}.RDS",
    tilesDir = "{outputDir}/maps/{voi}/{model_name}_{depth_name}_{transformation}_{use_rfe}_{do_tune}",
    outputDir = NULL,
    depth_name = "0-20",
    transformation = "notransform",
    use_rfe = "dorfe", do_tune = "tune",
    multiply,
    gdal_options,
    datatype_geotiff, depths_3D = NULL,
    covariate_pattern = ".tif$",
    NA_mask_flag = NULL,
    rndseed,
    model_name = "rangerquantreg",
    model_options = list(
        parsnip_engine = "ranger",
        parsnip_importance = "impurity",
        parsnip_quantreg = TRUE,
        parsnip_mode = "regression",
        parsnip_mtry_range_min_multiplier = 0.5,
        parsnip_mtry_range_max_multiplier = 3,
        parsnip_trees_range = c(500, 1000),
        ntree_default = 500,
        stats = c("mean", "Q0.5", "Q0.05", "Q0.95"),
        stats_to_log_backtansform = c("mean"),
        stats_to_exp = c("Q0.5", "Q0.05", "Q0.95")
    ), overwrite = TRUE) {
    # browser()
    Sys.setenv(rndseed = rndseed)
    tilesDir <- glue::glue(tilesDir, .null = ".")
    tilesFile <- glue::glue(tilesFile, .null = ".")
    rfeCovariatesFile <- glue::glue(rfeCovariatesFile, .null = ".")
    modelFittedFile <- glue::glue(modelFittedFile, .null = ".")

    tiles_df <- utils::read.csv(tilesFile)
    mdl_fit <- readRDS(modelFittedFile)
    tilePredicted <- file.path(
        tilesDir, # paste0("ntiles_", nsplits),
        "tiles",
        paste0(sprintf("%05d", tile_number), ".tif")
    )
    writeLines(sprintf("Processing %s...", tilePredicted))

    files_exist <- unlist(lapply(list(tilePredicted), file.exists))
    if (all(files_exist) & !overwrite) {
        writeLines("all output files already exist, skipping this step, use orverwrite=TRUE to overwrite files")
    } else {
        covars_files <- list.files(covarsDir,
            pattern = covariate_pattern,
            full.names = TRUE
        )

        nsplits <- nrow(tiles_df)
        extent <- terra::ext(as.numeric(tiles_df[tile_number, ]))

        # # Selecting only the independent variables that the model is going to need
        modeled_covariates <- get_model_independent_vars(mdl_fit, model_name)
        covariates_names_likeindf <- make.names(tools::file_path_sans_ext(basename(covars_files)))
        modeled_covariates_idx <- covariates_names_likeindf %in% modeled_covariates
        # modeled_covariates_names <- modeled_covariates[modeled_covariates_idx]
        covars_files <- covars_files[modeled_covariates_idx]
        covars_files_names <- make.names(tools::file_path_sans_ext(basename(covars_files)))

        if (!missing(maskFile)) {
            mask <- terra::rast(maskFile,lyrs=1)
            if (!is.null(NA_mask_flag)) terra::NAflag(mask) <- NA_mask_flag
            terra::window(mask) <- NULL
            terra::window(mask) <- terra::ext(extent)
        }
        # if rast works it will read the tile window as a stack of SpatRasters
        # otherwise it will read the tile window one by one, reprojecting and
        # resampling if necessary. If terra::rast fails, covars_files will be
        # a list of file paths, otherwise it will be a SpatRaster
        t <- try(covars_files <- terra::rast(covars_files))
        if ("try-error" %in% class(t)) {
            writeLines("Reading covariates files one by one")
        }
        indep_vars <- lapply(covars_files, read_spatraster_window,
            mask = mask,
            extent = extent, outformat = "SpatRaster", xy = TRUE,
            mask_output = FALSE,
            resample_output = TRUE,
            reproject_output = TRUE
        )

        indep_vars <- terra::rast(indep_vars)

        t <- try(indep_vars <- terra::mask(indep_vars, mask))
        if ("try-error" %in% class(t)) {
            indep_vars <- terra::resample(indep_vars, mask)
            indep_vars <- terra::mask(indep_vars, mask)
        }
        names(indep_vars) <- covars_files_names

        indep_vars <- terra::as.data.frame(indep_vars, xy = TRUE, na.rm = TRUE)


        if (dim(indep_vars)[1] == 0) {
            message(paste0("Tile ",tile_number," is empty"))

            if (depth_name == "3D") {
                bandnames <- sort(as.vector(outer(paste(voi, sort(model_options$stats), sep = "_"),
                    getDepthsFolders(x = depths_3D), paste,
                    sep = "_"
                )))
            } else {
                bandnames <- paste(voi, sort(model_options$stats), paste0(depth_name, "cm"), sep = "_")
            }
            terra::values(mask) <- NA
            pred_raster <- rast(unlist(lapply(1:length(bandnames), function(x, y) {
                return(y)
            }, mask)))
            names(pred_raster) <- bandnames

            terra::crs(pred_raster) <- terra::crs(mask)
            dir.create(dirname(tilePredicted), showWarnings = FALSE, recursive = TRUE)
            terra::writeRaster(pred_raster, tilePredicted,
                overwrite = TRUE,
                wopt = list(
                    gdal = gdal_options,
                    datatype = datatype_geotiff
                )
            )
        } else {
            if (depth_name == "3D") {
                pred_raster <- lapply(1:length(getDepthsMidPoints(x = depths_3D)), function(i) {
                    indep_vars$depth <- getDepthsMidPoints(x = depths_3D)[i]
                    depth_folder <- getDepthsFolders(x = depths_3D)[i]
                    pred_raster <- predict_dataframe(
                        indep_vars = indep_vars, mdl_fit = mdl_fit,
                        resx = terra::res(mask)[1], resy = terra::res(mask)[2],
                        crs = terra::crs(mask),
                        model_name = model_name,
                        transformation = transformation,
                        multiply = multiply,
                        model_options = model_options, seed = rndseed
                    )
                    names(pred_raster) <- paste(voi, names(pred_raster), depth_folder, sep = "_")
                    return(pred_raster)
                })

                pred_raster <- terra::rast(pred_raster)
                dir.create(dirname(tilePredicted), showWarnings = FALSE, recursive = TRUE)

                if (terra::ext(pred_raster) != terra::ext(extent)) {
                    pred_raster <- terra::extend(pred_raster, terra::ext(extent))
                }
                terra::writeRaster(pred_raster, tilePredicted,
                    overwrite = TRUE, # orverwrite_tiff
                    wopt = list(
                        gdal = gdal_options, # _multiband
                        datatype = datatype_geotiff
                    )
                )
            } else {
                pred_raster <- predict_dataframe(
                    indep_vars = indep_vars, mdl_fit = mdl_fit,
                    resx = terra::res(mask)[1], resy = terra::res(mask)[2],
                    crs = terra::crs(mask),
                    model_name = model_name,
                    transformation = transformation,
                    multiply = multiply,
                    model_options = model_options, seed = rndseed
                )

                names(pred_raster) <- paste(voi, names(pred_raster), paste0(depth_name, "cm"), sep = "_")
                dir.create(dirname(tilePredicted), showWarnings = FALSE, recursive = TRUE)
                if (terra::ext(pred_raster) != terra::ext(extent)) {
                    pred_raster <- terra::extend(pred_raster, terra::ext(extent))
                }
                terra::writeRaster(pred_raster, tilePredicted,
                    overwrite = TRUE,
                    wopt = list(gdal = gdal_options, datatype = datatype_geotiff)
                )
            }
        }
        rm(pred_raster)
        rm(indep_vars)
    }
}
