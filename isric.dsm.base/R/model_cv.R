#' Run cross-validation for quantile regression using Random Forest
#'
#' Run cross-validation on the given regression matrix.
#'
#' @param voi Name of the variable of interest
#' @param regressionMatrixFile Path to the .csv file with the regression matrix
#' @param rfeCovariatesFile Path to the .txt file with the covariates to use in case of RFE
#' @param bestParamFile Path to the .csv file with the best hyperparameters values to use in case do_tune is set to "notune"
#' @param cvMetricsFile Path to the .csv file where to save the cross-validation metrics output
#' @param cvPredictObserveFile Path to the .csv file where to save the cross-validation predictions and observations output
#' @param outputDir Character. Optional. Used if input and or output files are not given
#' @param model_name Character. The name of the model
#' @param use_rfe Specifies whether to apply Recursive Feature Elimination. Options are "dorfe" for RFE or "norfe" for no RFE.
#' @param depth_name Name of the column containing the depth information in the regression matrix
#' @param depth_column_name Name to use for the additional column to be created when using 3D depth information
#' @param do_tune Specifies whether to tune hyperparameters. Options are "tune" or "notune".
#' @param transformation Specifies whether to apply a log transformation to the dependent variable. Options are "log" or "notransform"
#' @param pid_name Name of the column containing the plot ID in the regression matrix
#' @param lyrid_name Name of the column containing the layer ID in the regression matrix
#' @param fold_name Name of the column containing the fold ID in the regression matrix
#' @param cores Numeric. Number of cores to be used for parallel processing. Default to 1, sequential
#' @param flag_validation_name Name of the column containing the validation flag in the regression matrix
#' @param flag_tuning_name Name of the column containing the tuning flag in the regression matrix
#' @param flag_rfe_name Name of the column containing the RFE flag in the regression matrix
#' @param model_options List. list of model options, including hyperparameters
#' @param rndseed Numeric. Value used to initialize the random number generator
#' @param overwrite logical. if TRUE overwrite outputs. if FALSE function exits if all outpus are present. Default to TRUE
#'
#' @importFrom tidyselect any_of all_of
#' @importFrom tune control_resamples collect_predictions fit_resamples
#' @importFrom dplyr mutate across case_when select sym row_number left_join rename sym bind_rows
#' @importFrom rsample group_vfold_cv get_rsplit complement manual_rset
#' @importFrom parsnip rand_forest set_engine set_mode
#' @importFrom workflows workflow add_model add_formula
#' @importFrom yardstick new_numeric_metric rmse mae rsq metric_set
#' @importFrom glue glue
#' @importFrom doFuture registerDoFuture
#' @importFrom future cluster plan sequential multisession
#'
#' @return Outputs the cross-validation metrics and the cross-validation predictions and observations in two separate .csv files.
#'
#' @export
model_cv <- function(
    voi,
    regressionMatrixFile = "{outputDir}/model/{voi}/regression_matrix_{depth_name}.csv",
    rfeCovariatesFile = "{outputDir}/model/{voi}/rfe_{depth_name}_{transformation}.csv",
    bestParamFile = "{outputDir}/model/{voi}/bestparam_{model_name}_{depth_name}_{transformation}_{use_rfe}.csv",
    cvMetricsFile = "{outputDir}/model/{voi}/cv-metrics_{model_name}_{depth_name}_{transformation}_{use_rfe}_{do_tune}.csv",
    cvPredictObserveFile = "{outputDir}/model/{voi}/cv-predict-observe_{model_name}_{depth_name}_{transformation}_{use_rfe}_{do_tune}.csv",
    outputDir = NULL,
    model_name = "rangerquantreg",
    use_rfe = "dorfe",
    do_tune = "tune",
    transformation = "notransform",
    pid_name = "pid", lyrid_name = "lyrid", fold_name = "fold",
    depth_name = "0-20",
    depth_column_name = "depth",
    cores = 1,
    flag_validation_name = "NULL",
    flag_tuning_name = "NULL",
    flag_rfe_name = "NULL",
    model_options = list(
        parsnip_engine = "ranger",
        parsnip_importance = "impurity",
        parsnip_quantreg = TRUE,
        parsnip_mode = "regression",
        parsnip_mtry_range_min_multiplier = 0.5,
        parsnip_mtry_range_max_multiplier = 3,
        parsnip_trees_range = c(500, 1000),
        ntree_default = 500,
        stats = c("mean", "Q0.5", "Q0.05", "Q0.95"),
        stats_to_log_backtansform = c("mean"),
        stats_to_exp = c("Q0.5", "Q0.05", "Q0.95")
    ), rndseed = 982374923, overwrite = TRUE) {
    # Read the regression matrix
    writeLines("++ started: model_cv")
    Sys.setenv(rndseed = rndseed)

    cvPredictObserveFile <- glue::glue(cvPredictObserveFile, .null = ".")
    cvMetricsFile <- glue::glue(cvMetricsFile, .null = ".")
    bestParamFile <- glue::glue(bestParamFile, .null = ".")

    files_exist <- unlist(lapply(list(cvPredictObserveFile, cvMetricsFile), file.exists))
    if (all(files_exist) & !overwrite) {
        writeLines("all output files already exist, skipping this step, use orverwrite=TRUE to overwrite files")
    } else {
        regression_matrix <- read.csv(glue::glue(regressionMatrixFile, .null = "."))

        covariates_name <- colnames(regression_matrix)[!colnames(regression_matrix) %in%
            c(
                pid_name, fold_name, lyrid_name, voi, depth_column_name,
                flag_validation_name,
                flag_tuning_name,
                flag_rfe_name
            )]

        if (use_rfe == "dorfe") {
            covariates_rfe <- readLines(glue::glue(rfeCovariatesFile, .null = "."))
            covariates_name <- covariates_name[covariates_name %in% covariates_rfe]
        }

        if (depth_name == "3D") {
            covariates_name <- c(covariates_name, depth_column_name)
        }


        # Prepare a formula to apply log transformation to the soil variable of interest. Apply log transformation ff the log transformation parameter (defined in config) is active
        if (transformation == "notransform") {
            voi_modif <- voi
            frml <- as.formula(paste0(
                voi, "~",
                paste0(covariates_name, collapse = "+")
            ))
        } else {
            voi_modif <- paste0(transformation, "(", voi, ")")
            frml <- as.formula(paste0(
                voi_modif, "~",
                paste0(covariates_name, collapse = "+")
            ))
        }

        if (transformation == "log") {
            # Replace values == 0 with 0.0001 (this avoids errors when transformation
            # is applied)
            # regression_matrix[regression_matrix[[voi]] == 0, voi] <- 0.0001
            regression_matrix <- regression_matrix %>%
                dplyr::mutate(dplyr::across(
                    tidyselect::all_of(voi),
                    ~ dplyr::case_when(. == 0 ~ 0.0001, .default = .)
                ))
        }
        # Prepare the dataset for cross validation. The groups used to split the data are defined in the column **fold** of the dataframe and have been created beforehand by ISRIC
        regression_matrix <- regression_matrix %>%
            dplyr::mutate(dplyr::across(
                tidyselect::any_of(flag_validation_name),
                ~ dplyr::case_when(. == "" ~ NA, .default = .)
            ))

        if (flag_validation_name %in% names(regression_matrix)) {
            regression_matrix[regression_matrix[flag_validation_name] == 0, fold_name] <- NA
        }

        splitted_regr_matr <- rsample::group_vfold_cv(data = regression_matrix, group = fold_name)

        resamples <- unlist(lapply(seq(nrow(splitted_regr_matr)), function(i, rsplit, regression_matrix) {
            if (any(is.na(regression_matrix[[fold_name]]))) {
                return(!all(which(is.na(regression_matrix[[fold_name]])) %in% rsample::complement(rsample::get_rsplit(rsplit, i))))
            } else {
                return(TRUE)
            }
        }, rsplit = splitted_regr_matr, regression_matrix = regression_matrix))

        split <- splitted_regr_matr[resamples, ]

        splitted_regr_matr <- rsample::manual_rset(split$splits, split$id)

        rf_wf <- get(paste0(model_name, "_model_cv_engine"))(frml = frml,
            bestParamFile = bestParamFile, covariates_name = covariates_name,
            do_tune = do_tune, model_options = model_options, rndseed = rndseed)

        # Define a custom metric to assess model performance, in this case ccc with bias and mec (model efficiency coefficient). These have been pre-defined in the ***ccc_with_bias.R*** and ***R/MEC.R*** scripts
        mec <- yardstick::new_numeric_metric(mec, direction = "maximize")
        ccc_with_bias <- yardstick::new_numeric_metric(ccc_with_bias, "maximize")

        multi_metric <- yardstick::metric_set(
            yardstick::rmse, yardstick::mae,
            yardstick::rsq, ccc_with_bias, mec
        )

        if (cores > 1) {
            # future::plan(future::multisession, workers = as.numeric(cores))
            doFuture::registerDoFuture()
            future::plan(future::cluster, workers = as.numeric(cores))
        }

        # Run the cross validation using the created workflow and the specified metrics
        rf_fit_rs <- rf_wf %>%
            tune::fit_resamples(splitted_regr_matr,
                metrics = multi_metric,
                control = tune::control_resamples(save_pred = TRUE)
            )
        if (cores > 1) {
            future::plan(future::sequential) # shut down multisessions
        }
        rm(rf_wf)
        cv_predict_observe <- tune::collect_predictions(rf_fit_rs) %>%
            dplyr::select(-c(".config")) %>%
            dplyr::rename(resample_id = id)

        pids <- dplyr::select(
            regression_matrix, !!dplyr::sym(pid_name),
            tidyselect::any_of(c(lyrid_name, fold_name))
        ) %>%
            dplyr::mutate(.row = dplyr::row_number())

        cv_predict_observe <- dplyr::left_join(pids, cv_predict_observe) %>%
            dplyr::select(-.row) %>%
            dplyr::rename(pred = .pred)

        cv_metrics <- cv_predict_observe %>% multi_metric(truth = !!dplyr::sym(voi_modif), estimate = pred)
        cv_metrics <- dplyr::select(cv_metrics, metric = .metric, value = .estimate)
        cv_metrics$transformation <- transformation

        if (transformation != "notransform") {
            backtransformed <- data.frame(
                regression_matrix[[voi]],
                get(paste0(transformation, "_backtransform"))(cv_predict_observe[["pred"]])
            )
            names(backtransformed) <- c(voi, "pred")
            backtransformed_metrics <- backtransformed %>% multi_metric(truth = !!dplyr::sym(voi), estimate = pred)
            backtransformed_metrics <- dplyr::select(backtransformed_metrics, metric = .metric, value = .estimate)
            backtransformed_metrics$transformation <- "backtransformed"

            cv_metrics <- dplyr::bind_rows(cv_metrics, backtransformed_metrics)
        }
        # cv_metrics <- tune::collect_metrics(rf_fit_rs)
        # cv_metrics <- dplyr::select(cv_metrics, metric = .metric, value = mean)
        # Save the cross validation results into an .csv object for later evaluation
        dir.create(dirname(cvMetricsFile), showWarnings = FALSE, recursive = TRUE)
        write.csv(cv_metrics, cvMetricsFile, row.names = FALSE, quote = FALSE)

        dir.create(dirname(cvPredictObserveFile),
            showWarnings = FALSE,
            recursive = TRUE
        )
        write.csv(cv_predict_observe, cvPredictObserveFile,
            row.names = FALSE,
            quote = FALSE
        )

        # Output Global environment variables as a .genv file having the same name as the main output file. The file can be loaded with source()
        write_genv(refFile = cvMetricsFile)
        rm(rf_fit_rs)
        rm(cv_predict_observe)
    }
    writeLines("-- ended: model_cv")
    writeLines("   ")
}
