#' predict_qrf
#'
#' Predict a quantile regression random forest model (package ranger)
#' Wrapper around predict method for quantile regression random forest model
#' (package ranger).
#'
#' @param df The data.frame with the independent variables. Names musth match
#' names in the model
#' @param model The ranger object with the fitted qrf model
#' @param stat Character vector. The statistic to predict. "mean" and/or
#' quantiles in the form of e.g. "Q0.05", "Q0.5", "Q0.95"
#'
#' @return A data.frame with the predictions and the column name as the
#' specified stat(s)
#'
#' @export
#'
#' @import ranger
#'
#'

comprangerquantreg_predict <- function(df, model, pred_num.trees = 10,seed, ...) {
    #browser()
    # realizations <- lapply(stat, function(x) {
    #     set.seed(as.numeric(x))
    #     pred_mean <- predict(model, df, type = "response")
    #     pred_mean <- as.data.frame(pred_mean$predictions)
    #     names(pred_mean) <- paste0("r", x)
    #     return(pred_mean)
    # })
    # pred_mean <- predict(model, df,
    #     predict.all = TRUE,
    #     num.trees = pred_num.trees, type = "response"
    # )
    # pred_mean <- as.data.frame(pred_mean$predictions)

    # # pred_mean <- dplyr::bind_cols(realizations)
    # return(data.frame(pred_mean))

    #browser()
    num.trees <- model$num.trees
    if (!is.null(pred_num.trees)) {
        num.trees <- pred_num.trees
    }
    terminal.nodes <- predict(model, df, type = "terminalNodes",seed = seed)$predictions + 1
    node.values <- 0 * terminal.nodes
    for (tree in 1:num.trees) {
        node.values[, tree] <- model$random.node.values[terminal.nodes[, tree], tree]
    }
    
    #colnames(result$predictions) <- paste0("tree_", 1:num.trees)
    return(data.frame(node.values[,1:num.trees]))
}
