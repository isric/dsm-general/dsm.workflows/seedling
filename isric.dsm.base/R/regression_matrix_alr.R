#' Regression Matrix with ALR Transformation
#'
#' This function reads regression matrix files and performs an additive log-ratio (ALR) transformation on specified variables of interest (voi).
#' The resulting ALR-transformed variables are then written to a regression matrix in their own folder.
#' The number of alr transformed variables is the number of variables of interest minus one.
#' The function filters out rows where any ALR-transformed variable is equal to infinity.
#'
#' @param voi A character vector specifying the variables of interest to apply the ALR transformation on.
#' @param regressionMatrixFile A string indicating the file path of the regression matrix file.
#' @param profilesOverlayFile A string indicating the file path of the spatial overlay file.
#' @param pid_name A character specifying the name of the identifier column in the regression matrix and spatial overlay.
#' @param lyrid_name A character specifying the name of the Lyrid column in the regression matrix and spatial overlay.
#' @param depth_column_name A character specifying the name of the depth column in the regression matrix (default is "depth").
#' @param fold_name A character specifying the name of the fold column in the regression matrix (default is "fold").
#' @param outputDir A string specifying the output directory for the processed regression matrix (default is NULL).
#' @param depth_name A string specifying the name of the depth column in the regression matrix (default is the "0-20").
#' @param alr_prefix A string specifying the prefix for the ALR-transformed variables (default is "alr").
#' @return A character vector containing the names of the ALR-transformed variables appended to the regression matrix.
#' @examples
#' voi <- c("var1", "var2")
#' regressionMatrixFile <- "regression_matrix.csv"
#' profilesOverlayFile <- "profiles_overlay.csv"
#' pid_name <- "pid"
#' lyrid_name <- "lyrid"
#' alr_names <- regression_matrix_alr(voi, regressionMatrixFile, profilesOverlayFile, pid_name, lyrid_name)
#' @export
#'
#' @importFrom glue glue
#' @importFrom dplyr select bind_cols
#' @importFrom tidyselect any_of all_of
#' @importFrom purrr reduce
regression_matrix_alr <- function(
    voi,
    regressionMatrixFile,
    profilesOverlayFile,
    pid_name,
    lyrid_name,
    depth_column_name = "depth",
    fold_name = "fold",
    outputDir = NULL,
    depth_name = "0-20",
    alr_prefix = "alr") {
  # Read and filter regression matrix files
  regression_matrix <- lapply(voi, function(voi) {
    regression_matrix <- read.csv(glue::glue(regressionMatrixFile, .null = "."))
    regression_matrix <- dplyr::select(
      regression_matrix,
      tidyselect::any_of(c(
        pid_name,
        fold_name,
        lyrid_name,
        voi,
        depth_column_name
      ))
    )
  })

  regression_matrix <- purrr::reduce(regression_matrix, dplyr::left_join)

  # Read spatial overlay file
  spatial_overlay <- read.csv(glue::glue(profilesOverlayFile, .null = "."))

  covariates_name <- colnames(spatial_overlay)[
    !colnames(spatial_overlay) %in%
      c(pid_name, fold_name, lyrid_name, voi)
  ]

  # Perform ALR transformation on the selected variables
  regression_matrix <- regression_matrix %>%
    dplyr::mutate(dplyr::across(
      tidyselect::all_of(voi),
      ~ dplyr::case_when(. == 0 ~ 0.0001, .default = .)
    ))
  regression_matrix_ <- alr(dplyr::select(regression_matrix, tidyselect::any_of(voi)))

  alr_names <- paste0(alr_prefix, seq(length(names(regression_matrix_))))
  names(regression_matrix_) <- alr_names

  # Join regression matrix with spatial overlay
  ids <- dplyr::select(
    regression_matrix,
    tidyselect::any_of(c(pid_name, fold_name, lyrid_name, depth_column_name))
  )

  regression_matrix <- dplyr::left_join(
    dplyr::bind_cols(ids, regression_matrix_),
    spatial_overlay
  )

  rm(regression_matrix_)

  # Filter out rows with infinity values
  regression_matrix <- dplyr::filter(
    regression_matrix,
    dplyr::if_any(dplyr::any_of(alr_names), ~ . != Inf)
  )

  # Write processed regression matrix to file
  dummy <- lapply(alr_names, function(voi) {
    regression_matrix <- dplyr::select(
      regression_matrix,
      tidyselect::any_of(c(pid_name, fold_name, lyrid_name, voi, depth_column_name, covariates_name))
    )

    regressionMatrixFile <- glue::glue(regressionMatrixFile, .null = ".")
    dir.create(
      dirname(regressionMatrixFile),
      showWarnings = FALSE,
      recursive = TRUE
    )

    write.csv(
      regression_matrix,
      regressionMatrixFile,
      row.names = FALSE,
      quote = FALSE
    )
  })

  return(alr_names)
}
