#' overlay_locations_raster
#' 
#' Overlay point locations and a raster extracting the raster values at those
#' locations
#'
#' @param rastPath Character. Path to raster
#' @param locations Character or SpatVector object containing locations.
#' If character: path to csv, easting_name, northing_name, crs_profiles have
#' to bespecified
#' @param easting_name Character. Only used if locations == Character. 
#' Name for the easting column
#' @param northing_name Character. Only used if locations == Character. 
#' Name for the northing column 
#' @param crs_profiles Character. The CRS for the locations, can be EPSG code
#' WKT, PROJ4 string (anything terra vect() function wuold accept)
#' @param pid_name Character. Name of the id colum. Used to avoid selecting
#' other not necessary variables 
#' 
#' @return A data.frame with the values of the locations csv and the values
#' of the raster at those locations
#' 
#' @export
#' 
#' @importFrom terra project rast extract
#' @importFrom tools file_path_sans_ext
#'
#'


overlay_locations_raster <- function(rastPath, locations,
                                      easting_name = "x", 
                                      northing_name = "y",
                                      crs_profiles = NULL,
                                      pid_name = "pid") {
    rst <- terra::rast(rastPath)
    names(rst) <- tools::file_path_sans_ext(basename(rastPath))
    if (is.character(locations)) {
        locations <- vect(read.csv(profilesStandardFile),
            geom = c(easting_name, northing_name),
            crs = crs_profiles
        )
    }
    locations <- locations[,pid_name]
    names(rst) <- tools::file_path_sans_ext(basename(rastPath))
    locations <- terra::project(locations, rst)
    spatial_overlay <- terra::extract(rst, locations,
        na.rm = TRUE, bind = TRUE, ID = FALSE
    )
    return(as.data.frame(spatial_overlay))
}