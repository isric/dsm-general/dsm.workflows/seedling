% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/predict_tiles_compositional.R
\name{predict_tiles_compositional}
\alias{predict_tiles_compositional}
\title{Predict a list of tiles backtrasforming fro additive log ratio (alr) transformation}
\usage{
predict_tiles_compositional(
  voi,
  tilesFile = "{outputDir}/other/tiles.csv",
  tilesDir =
    "{outputDir}/maps/{voi}/{model_name}_{depth_name}_{transformation}_{use_rfe}_{do_tune}",
  outputDir = NULL,
  alr_names,
  depth_name = "0-20",
  transformation = "notransform",
  use_rfe = "dorfe",
  do_tune = "tune",
  alrinv_scale_factor = NULL,
  multiply,
  gdal_options,
  datatype_geotiff,
  depths_3D,
  covariate_pattern,
  NA_mask_flag = NULL,
  cores = 1,
  model_name = "comprangerquantreg",
  model_options = list(parsnip_engine = "ranger", parsnip_importance = "impurity",
    parsnip_quantreg = TRUE, parsnip_mode = "regression",
    parsnip_mtry_range_min_multiplier = 0.5, parsnip_mtry_range_max_multiplier = 3,
    parsnip_trees_range = c(500, 1000), parsnip_minn_range = c(2, 10), ntree_default =
    500, stats = c("mean", "Q0.5", "Q0.05", "Q0.95"), pred_num.trees = 100,
    stats_to_log_backtansform = c("mean"), stats_to_exp = c("Q0.5", "Q0.05", "Q0.95")),
  overwrite = TRUE
)
}
\arguments{
\item{voi}{A character vector specifying the variable of interest.}

\item{tilesFile}{character string giving the path to a csv file containing information about the tiles to be predicted.}

\item{tilesDir}{The directory path where the output tile rasters will be stored. Default is "{outputDir}/maps/{voi}/{model_name}_{depth_name}_{transformation}_{use_rfe}_{do_tune}".}

\item{outputDir}{The output directory path. If NULL, the current working directory will be used.}

\item{alr_names}{A character vector specifying the names of the additive log-ratio (ALR) variables.}

\item{depth_name}{The depth name for the rasters. Default is "0-20".}

\item{transformation}{The transformation applied to the rasters. Default is "notransform".}

\item{use_rfe}{A character indicating whether Recursive Feature Elimination (RFE) is used. Default is "dorfe".}

\item{do_tune}{A character indicating whether hyperparameter tuning is performed. Default is "tune".}

\item{alrinv_scale_factor}{Numeric. Output matrix is multiplied by this number.
This is useful when dealing with compositional data values that range above
1. Use 100 if your data ranges from 0 to 100,
use 1 if your data ranges from 0 to 1.}

\item{multiply}{A numeric value used to multiply the predicted rasters. Default is NULL.}

\item{gdal_options}{A list of GDAL options for writing the rasters. Default is NULL.}

\item{datatype_geotiff}{The datatype for the output GeoTIFF rasters. Default is NULL.}

\item{depths_3D}{A 3D depth array. Default is NULL.}

\item{covariate_pattern}{A pattern for covariate selection. Default is NULL.}

\item{NA_mask_flag}{Numeric value indicating on which values to use as NA mask. Default is NULL (nas taken from raster metadata)}

\item{cores}{The number of cores to be used. Default is 1.}

\item{model_name}{The name of the model. Default is "comprangerquantreg".}

\item{model_options}{Additional options for the model. Default is NULL.}

\item{overwrite}{logical. if TRUE overwrite outputs. if FALSE function exits if all outpus are present. Default to TRUE}

\item{expand_grid_realizations}{A logical value indicating whether to expand the grid of realizations. Default is FALSE.}
}
\value{
None.
}
\description{
This function uses a the predictions from one or more alr and bactransforms them in n+1 variables
}
