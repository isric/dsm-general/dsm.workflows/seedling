library(isric.dsm.base)

stats <- sort(c("mean", "Q0.5", "Q0.05", "Q0.95"))
depths_3D <- "0,5,15,30,60,100,200"
voi = "pH"
sort(as.vector(outer(paste(voi, sort(stats), sep = "_"),
                    getDepthsFolders(x = depths_3D), paste,
                    sep = "_"
                )))


library(terra)

voi = "sand" #define variable of interest
depth = "0-5cm"
quantile = "Q0.5"

output_filename = "output.tif"

#in_crs="EPSG:4326" # CRS of the bounding box. use EPSG code or WKT 
#in this case the in_crs is not needed as it is already Interrupted Goode Homolosine (igh)

xmin = -337500.000 #bounding box xmin
xmax = 152500.000 #bounding box xmax
ymin = 527500.000 #bounding box ymin
ymax = 1242500.000 #bounding box ymax

bb=ext(c(xmin,xmax,ymin,ymax))

igh = "ESRI:54052"

bb_igh = bb

# bb_igh= project(bb,from=crs(in_crs),to=crs(igh)) #only necessary where crs of bounding box is not igh

voi_layer = paste(voi,depth,quantile, sep="_") # layer of interest

rstFile = paste0("/vsicurl/https://files.isric.org/soilgrids/latest/data/", voi, '/', voi_layer,'.vrt')


sand = rast(rstFile) # voi for whole world
crs(sand) = crs(igh)

window(sand) <- NULL
window(sand) = bb_igh # get just roi
plot(sand) #quick visual check


voi = "silt" #define variable of interest
voi_layer = paste(voi,depth,quantile, sep="_") # layer of interest
rstFile = paste0("/vsicurl/https://files.isric.org/soilgrids/latest/data/", voi, '/', voi_layer,'.vrt')

silt = rast(rstFile) # voi for whole world
crs(silt) = crs(igh)

window(silt) <- NULL
window(silt) = bb_igh # get just roi
plot(silt) #quick visual check


voi = "clay" #define variable of interest
voi_layer = paste(voi,depth,quantile, sep="_") # layer of interest
rstFile = paste0("/vsicurl/https://files.isric.org/soilgrids/latest/data/", voi, '/', voi_layer,'.vrt')

clay = rast(rstFile) # voi for whole world
crs(clay) = crs(igh)

window(clay) <- NULL
window(clay) = bb_igh # get just roi
plot(clay) #quick visual check

sand+silt+clay

plot(sand+silt+clay)

library(tdigest)
set.seed(1492)
x <- sample(0:100, 1000000, replace = TRUE)
td <- tdigest(x, 1000)
probs = c(0.5,0.05,0.95)
tquantile(td, probs = probs)
quantile(td,probs = probs)


sand <- terra::rast("./output_Madagascar/maps/fsand/comprangerquantreg_0-20_notransform_dorfe_tune/fsand_mean_0-20cm.tif")
silt <- terra::rast("./output_Madagascar/maps/fsilt/comprangerquantreg_0-20_notransform_dorfe_tune/fsilt_mean_0-20cm.tif")
clay <- terra::rast("./output_Madagascar/maps/fclay/comprangerquantreg_0-20_notransform_dorfe_tune/fclay_mean_0-20cm.tif")

terra::plot(sand+silt+clay)

sand+silt+clay

terra::plot(terra::rast(c("output_Madagascar/maps/fsand/comprangerquantreg_0-20_notransform_dorfe_tune/fsand_mean_0-20cm.tif","output_Madagascar/maps/fsilt/comprang
erquantreg_0-20_notransform_dorfe_tune/fsilt_mean_0-20cm.tif","output_Madagascar/maps/fclay/comprangerquantreg_0-20_notransform_dorfe_tune/fclay_mean_0-20cm.tif")))

library(terra)
sand_median <- terra::rast("./output_Madagascar/maps/fsand/comprangerquantreg_0-20_notransform_dorfe_tune/fsand_Q0.5_0-20cm.tif")
silt_median <- terra::rast("./output_Madagascar/maps/fsilt/comprangerquantreg_0-20_notransform_dorfe_tune/fsilt_Q0.5_0-20cm.tif")
clay_median <- terra::rast("./output_Madagascar/maps/fclay/comprangerquantreg_0-20_notransform_dorfe_tune/fclay_Q0.5_0-20cm.tif")

summary(sand_median)

terra::plot(sand_median+silt_median+clay_median)


sand_median+silt_median+clay_median
sand+silt+clay

sand_Q0.05 <- terra::rast("./output_Madagascar/maps/fsand/comprangerquantreg_0-20_notransform_dorfe_tune/fsand_Q0.05_0-20cm.tif")
silt_Q0.05 <- terra::rast("./output_Madagascar/maps/fsilt/comprangerquantreg_0-20_notransform_dorfe_tune/fsilt_Q0.05_0-20cm.tif")
clay_Q0.05 <- terra::rast("./output_Madagascar/maps/fclay/comprangerquantreg_0-20_notransform_dorfe_tune/fclay_Q0.05_0-20cm.tif")

sand_Q0.05+silt_Q0.05+clay_Q0.05
terra::plot(sand_Q0.05+silt_Q0.05+clay_Q0.05)

summary(sand_Q0.05)



sand_Q0.95 <- terra::rast("./output_Madagascar/maps/fsand/comprangerquantreg_0-20_notransform_dorfe_tune/fsand_Q0.95_0-20cm.tif")
silt_Q0.95 <- terra::rast("./output_Madagascar/maps/fsilt/comprangerquantreg_0-20_notransform_dorfe_tune/fsilt_Q0.95_0-20cm.tif")
clay_Q0.95 <- terra::rast("./output_Madagascar/maps/fclay/comprangerquantreg_0-20_notransform_dorfe_tune/fclay_Q0.95_0-20cm.tif")

terra::plot(sand_Q0.95+silt_Q0.95+clay_Q0.95)

summary(sand_Q0.95)


summary(terra::rast("./output_Madagascar/maps/fsand/comprangerquantreg_0-20_notransform_dorfe_tune/fsand_Q0.95_0-20cm.tif"))
summary(terra::rast("./output_Madagascar/maps/fsand/comprangerquantreg_0-20_notransform_dorfe_tune/fsand_Q0.05_0-20cm.tif"))
summary(terra::rast("./output_Madagascar/maps/fsand/comprangerquantreg_0-20_notransform_dorfe_tune/fsand_Q0.5_0-20cm.tif"))

plot(
c(terra::rast("../output_Madagascar/maps/fclay/comprangerquantreg_0-20_notransform_dorfe_tune/fclay_Q0.95_0-20cm.tif"),
terra::rast("../output_Madagascar/maps/fclay/comprangerquantreg_0-20_notransform_dorfe_tune/fclay_Q0.05_0-20cm.tif"),
terra::rast("../output_Madagascar/maps/fclay/comprangerquantreg_0-20_notransform_dorfe_tune/fclay_Q0.5_0-20cm.tif"),
terra::rast("../output_Madagascar/maps/fclay/comprangerquantreg_0-20_notransform_dorfe_tune/fclay_mean_0-20cm.tif")))


######################
library(terra)

alr1 <- terra::rast("output_Madagascar/maps/alr1/comprangerquantreg_0-20_notransform_dorfe_tune/tiles/00116.tif")
alr2 <- terra::rast("output_Madagascar/maps/alr2/comprangerquantreg_0-20_notransform_dorfe_tune/tiles/00116.tif")

alr_names <- c("alr1","alr2")
n_alr_names <- length(alr_names)
stat = c("mean", "Q0.5", "Q0.05", "Q0.95")
voi <- c("sand", "silt", "clay")


alr <- rast(list(alr1,alr2))
####################################
####################################

# https://cran.r-project.org/web/packages/data.table/vignettes/datatable-reshape.html
# https://github.com/Rdatatable/data.table/issues/1717

library(data.table)
library(isric.dsm.base)
library(tdigest)

system.time ({

rasters_df <- as.data.frame(alr,cells = TRUE, xy = TRUE)
setDT(rasters_df)

depth = unique(unlist(strsplit(names(rasters_df),"_")[-c(1,2,3)])[grepl("cm",unlist(strsplit(names(rasters_df),"_")[-c(1,2,3)]))])

rasters_df = melt(rasters_df, measure = patterns(paste0("^",alr_names,".*","_",depth)), value.name = paste0(alr_names,"_",depth))

alr_names_depth = paste0(alr_names,"_",depth)
voi_depth = paste0(voi,"_",depth)
#rasters_df_alrinv <- alrinv(rasters_df[ , ..alr_names_depth])
#names(rasters_df_alrinv) <- voi_depth
#rasters_df <- data.table(rasters_df[,c("cell","x","y")],rasters_df_alrinv)

#rasters_df[, c("sand_0-20cm","silt_0-20cm","clay_0-20cm") := alrinv(rasters_df[ , c( "alr1_0-20cm","alr2_0-20cm")])]
#rasters_df[, (voi_depth) := alrinv(rasters_df[ , c( "alr1_0-20cm","alr2_0-20cm")])]
suppressWarnings(rasters_df[, (voi_depth) := alrinv((rasters_df[ , ..alr_names_depth]))])
rasters_df[,(alr_names_depth):=NULL]
rasters_df[,variable:=NULL]

rasters_df <- melt(rasters_df, id.vars = c("cell","x","y"))
#str(rasters_df)
#rasters_df[, c("voi", "depth") := tstrsplit(variable, "_", fixed=TRUE,type.convert = TRUE)]
#rasters_df[,variable:=NULL]
#str(rasters_df)
#rasters_df[, c("voi", "depth")  := lapply(.SD, as.factor), .SDcols = c("voi", "depth") ]
#str(rasters_df)

probs <- as.numeric(gsub("Q", "", stat[stat != "mean"]))

#system.time ({
rasters_df <- rasters_df[,lapply(.SD, function(v) {c(mean(v),tquantile(tdigest(v),probs=probs))}),.(cell,x,y,variable),.SDcols = c('value')]
#})

#rasters_df[,`:=` (lapply(.SD,  function(v) {c(mean(v),tquantile(tdigest(v),probs=probs))})),.(cell,x,y,voi,depth),.SDcols = c('value')]


stats_names <- factor(rep(stat,nrow(rasters_df)/length(stat)))
rasters_df[,"stat"] <- stats_names
#class(rasters_df[["variable"]])
str(rasters_df)

depth = c("0-20cm","20-30cm")

voiname <- paste0("sand","_",depth)


rasters_df[variable %in% voiname,]

rasters_df <- dcast(rasters_df[variable %in% voiname, c("x", "y", "variable", "stat", "value")], 
x + y ~ sapply( strsplit(as.character(variable),"_"), "[", 1 ) + stat + sapply( strsplit(as.character(variable),"_"), "[", 2 ), value.var = "value")
str(rasters_df)
#rasters_df <- dcast(rasters_df, cell + x + y ~ variable + stat, value.var = "value")

})

####################################
####################################
alr_vect <- as.vector(alr)

alr_mtrx <- matrix(alr_vect,ncol = length(alr_names),byrow = FALSE)
alr_inv <- isric.dsm.base::alrinv(alr_mtrx)
alr_df <- as.data.frame(array(alr_inv, dim = c(terra::ncol(alr)*terra::nrow(alr), terra::nlyr(alr)/n_alr_names*(n_alr_names + 1))))

df_num <- rep(seq(1, terra::nlyr(alr)/n_alr_names), time = terra::nlyr(alr)/n_alr_names)
df_names <- rep(voi, each = terra::nlyr(alr)/n_alr_names)
df_names_num <- unique(paste0(df_names, df_num))
names(alr_df) <- df_names_num


df_list <- lapply(voi, function(voi) {
    alr_df <- dplyr::select(alr_df, dplyr::starts_with(voi))

    if (any(stat == "mean")) {
        pred_mean <- data.frame(apply(alr_df, 1, mean, na.rm = TRUE))
        colnames(pred_mean) <- "mean"
    }

    if (any(stat != "mean")) {
        quantile <- as.numeric(gsub("Q", "", stat[stat != "mean"]))
        pred_Q <- data.frame(t(apply(alr_df, 1, quantile, probs = quantile, na.rm = TRUE)))
        colnames(pred_Q) <- stat[stat != "mean"]
    }

    if (exists(quote(pred_Q)) & exists(quote(pred_mean))) {
        return(data.frame(cbind(pred_mean, pred_Q)))
    } else if (exists(quote(pred_Q)) & !exists(quote(pred_mean))) {
        return(data.frame(pred_Q))
    } else if (!exists(quote(pred_Q)) & exists(quote(pred_mean))) {
        return(pred_mean)
    }
})

df_list[[1]][,"mean"] + df_list[[2]][,"mean"] + df_list[[3]][,"mean"]


dim(array(df_list[[1]], c(terra::ncol(alr),terra::nrow(alr),length(stat))))

terra::rast(array(df_list[[1]], c(terra::ncol(alr),terra::nrow(alr),length(stat))))


terra::rast(array(as.matrix(df_list[[1]]), dim = c(terra::ncol(alr),terra::nrow(alr),length(stat))))

dim()

dim(df_list[[1]])

summary(df_list[[1]][,"mean"])
summary(df_list[[2]][,"mean"])
summary(df_list[[3]][,"mean"])
