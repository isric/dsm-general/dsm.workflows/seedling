library(shiny)

# Define UI
ui <- fluidPage(
  titlePanel("Config.R Generator"),
  sidebarLayout(
    sidebarPanel(
      h3("Input/Output files definition"),
      textInput("outputDir", "Output Directory:", "./output/"),
      textInput("inputDir", "Input Directory:", "./input_example/"),
      textInput("srcDir", "Workflow Directory:", "./workflow/"),
      textInput("layersStandardFile", "Layers Standard File:", value = "geul_layers.csv"),
      textInput("profilesStandardFile", "Profiles Standard File:", value = "geul_profiles.csv"),
      textInput("covarsDir", "Covariates Directory:", "./input_example/covariates/"),
      textInput("maskFile", "Mask File:", value = "geul_mask.tif"),
      br(),
      h3("Variable of Interest"),
      selectInput("voi", "Variable of Interest:", choices = c("pb", "Humus", "pH")),
      br(),
      downloadButton("downloadConfig", "Download Config.R File")
    ),
    mainPanel(
      h4("Config.R File Contents:"),
      verbatimTextOutput("configOutput")
    )
  )
)

# Define server
server <- function(input, output) {
  
  # Define reactive expression for config.R file contents
  configContent <- reactive({
    paste0(
      "outputDir <- \"", input$outputDir, "\"\n",
      "inputDir <- \"", input$inputDir, "\"\n",
      "srcDir <- \"", input$srcDir, "\"\n",
      "layersStandardFile <- file.path(inputDir, \"points\", \"", input$layersStandardFile, "\")\n",
      "profilesStandardFile <- file.path(inputDir, \"points\", \"", input$profilesStandardFile, "\")\n",
      "covarsDir <- \"", input$covarsDir, "\"\n",
      "maskFile <- file.path(inputDir, \"other\", \"", input$maskFile, "\")\n",
      "voi <- \"", input$voi, "\""
    )
  })
  
  # Generate config.R file
  output$configOutput <- renderPrint({
    configContent()
  })
  
  # Download config.R file
  output$downloadConfig <- downloadHandler(
    filename = "config.R",
    content = function(file) {
      writeLines(configContent(), con = file)
    }
  )
}

# Run the app
shinyApp(ui = ui, server = server)



# library(shiny)

# # Define UI
# ui <- fluidPage(
#   titlePanel("Config.R Generator"),
#   sidebarLayout(
#     sidebarPanel(
#       h3("Input/Output files definition"),
#       textInput("outputDir", "Output Directory:", "./output/"),
#       textInput("inputDir", "Input Directory:", "./input_example/"),
#       textInput("srcDir", "Workflow Directory:", "./workflow/"),
#       fileInput("layersStandardFile", "Layers Standard File:"),
#       fileInput("profilesStandardFile", "Profiles Standard File:"),
#       textInput("covarsDir", "Covariates Directory:", "./input_example/covariates/"),
#       fileInput("maskFile", "Mask File:"),
#       br(),
#       h3("Variable of Interest"),
#       selectInput("voi", "Variable of Interest:", choices = c("pb", "Humus", "pH")),
#       br(),
#       downloadButton("downloadConfig", "Download Config.R File")
#     ),
#     mainPanel(
#       h4("Config.R File Contents:"),
#       verbatimTextOutput("configOutput")
#     )
#   )
# )

# # Define server
# server <- function(input, output) {
  
#   # Define reactive expression for config.R file contents
#   configContent <- reactive({
#     req(input$outputDir, input$inputDir, input$srcDir, input$covarsDir, input$voi)
#     req(input$layersStandardFile, input$profilesStandardFile, input$maskFile)
#     paste0(
#       "outputDir <- \"", input$outputDir, "\"\n",
#       "inputDir <- \"", input$inputDir, "\"\n",
#       "srcDir <- \"", input$srcDir, "\"\n",
#       "layersStandardFile <- file.path(input$inputDir, \"points\", \"", input$layersStandardFile$name, "\")\n",
#       "profilesStandardFile <- file.path(input$inputDir, \"points\", \"", input$profilesStandardFile$name, "\")\n",
#       "covarsDir <- \"", input$covarsDir, "\"\n",
#       "maskFile <- file.path(input$inputDir, \"other\", \"", input$maskFile$name, "\")\n",
#       "voi <- \"", input$voi, "\""
#     )
#   })
  
#   # Generate config.R file
#   output$configOutput <- renderPrint({
#     configContent()
#   })
  
#   # Download config.R file
#   output$downloadConfig <- downloadHandler(
#     filename = "config.R",
#     content = function(file) {
#       writeLines(configContent(), con = file)
#     }
#   )
# }

# # Run the app
# shinyApp(ui = ui, server = server)
